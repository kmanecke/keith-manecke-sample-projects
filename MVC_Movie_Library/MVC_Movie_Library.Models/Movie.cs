﻿using System;

namespace MVC_Movie_Library.Models
{
    public class Movie
    {
        public int MovieId { get; set; }
        public string Title { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string MpaaRating { get; set; }
        public string DirectorName { get; set; }
        public string Studio { get; set; }
        public int UserRating { get; set; }
        public string UserNotes { get; set; }
        public string Actors { get; set; }
        public string BorrowerName { get; set; }
        public DateTime? BorrowedDate { get; set; }
        public DateTime? ReturnedDate { get; set; }
    }
}
