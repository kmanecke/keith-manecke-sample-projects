﻿using System;
using System.Collections.Generic;
using System.Linq;
using MVC_Movie_Library.Services;
using MVC_Movie_Library.Models;
using NUnit.Framework;

namespace MVC_Movie_Library.Tests
{
    [TestFixture]
    public class MovieServiceTests
    {
        [Test]
        public void CanListMovieRecords()
        {
            var svc = new MovieService();
            var result = svc.List();

            Assert.AreEqual(5, result.ToList().Count);
        }

        [TestCase(1, "Joel Coen", "Doug Douglas")]
        [TestCase(2, "Shane Carruth", "Joe Jones")]
        public void CanGetSpecificMovieRecord(int movieId, string director, string borrower)
        {
            var svc = new MovieService();
            var movie = svc.Get(movieId);
            Assert.AreEqual(director, movie.DirectorName);
            Assert.AreEqual(borrower, movie.BorrowerName);
        }

        [TestCase(6, "Big Movie", "Bob Bobberson", 2)]
        [TestCase(7, "Cinema Masterpiece", "Monkey Man", 1)]
        public void CanAddMovieRecord(int id, string title, string director, int userRating)
        {
            var svc = new MovieService();
            var movie = new Movie();
            movie.Title = title;
            movie.DirectorName = director;
            movie.UserRating = userRating;
            svc.Add(movie);
            var newMovie = svc.Get(id);
            Assert.AreEqual(newMovie.Title, title);
            Assert.AreEqual(newMovie.DirectorName, director);
            Assert.AreEqual(newMovie.UserRating, userRating);
        }

        [TestCase(2, "Big Movie", "Bob Bobberson", 2)]
        [TestCase(3, "Cinema Masterpiece", "Monkey Man", 1)]
        public void CanEditMovie(int id, string title, string director, int userRating)
        {
            var svc = new MovieService();
            var movie = svc.Get(id);
            movie.Title = title;
            movie.DirectorName = director;
            movie.UserRating = userRating;
            svc.Edit(movie);
            var editedMovie = svc.Get(id);
            Assert.AreEqual(editedMovie.Title, title);
            Assert.AreEqual(editedMovie.DirectorName, director);
            Assert.AreEqual(editedMovie.UserRating, userRating);
        }

        [TestCase("jaw")]
        [TestCase("big")]
        public void CanSearchByTitle(string searchStr)
        {
            var svc = new MovieService();
            var result = svc.Search(searchStr);
            Assert.AreEqual(result.ToList().Count, 1);
        }

        [Test]
        public void CanDeleteMovie()
        {
            var svc = new MovieService();
            svc.Delete(2);
            var movies = svc.List();
            Assert.AreEqual(movies.ToList().Count, 4);
        }
    }
}
