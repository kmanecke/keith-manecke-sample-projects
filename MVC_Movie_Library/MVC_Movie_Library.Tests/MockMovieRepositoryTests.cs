﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_Movie_Library.Data.Repositories;
using MVC_Movie_Library.Models;
using NUnit.Framework;

namespace MVC_Movie_Library.Tests
{
    [TestFixture]
    public class MockMovieRepositoryTests
    {
        [Test]
        public void CanListMovieRecords()
        {
            var repo = new MockMovieRepository();
            var movies = repo.List();

            Assert.AreEqual(3, movies.ToList().Count);
        }

        [TestCase(1, "Joel Coen", "Keith Manecke")]
        [TestCase(2, "Shane Carruth", "Joe Jones")]
        public void CanGetSpecificMovieRecord(int movieId, string director, string borrower)
        {
            var repo = new MockMovieRepository();
            var movie = repo.Get(movieId);
            Assert.AreEqual(director, movie.DirectorName);
            Assert.AreEqual(borrower, movie.BorrowerName);
        }

        [TestCase(4, "Big Movie", "Bob Bobberson", 2)]
        [TestCase(5, "Cinema Masterpiece", "Monkey Man", 1)]
        public void CanAddMovieRecord(int id, string title, string director, int userRating)
        {
            var repo = new MockMovieRepository();
            var movie = new Movie();
            movie.Title = title;
            movie.DirectorName = director;
            movie.UserRating = userRating;
            repo.Add(movie);
            var newMovie = repo.Get(id);
            Assert.AreEqual(newMovie.Title, title);
            Assert.AreEqual(newMovie.DirectorName, director);
            Assert.AreEqual(newMovie.UserRating, userRating);
        }

        [TestCase(2, "Big Movie", "Bob Bobberson", 2)]
        [TestCase(3, "Cinema Masterpiece", "Monkey Man", 1)]
        public void CanEditMovie(int id, string title, string director, int userRating)
        {
            var repo = new MockMovieRepository();
            var movie = repo.Get(id);
            movie.Title = title;
            movie.DirectorName = director;
            movie.UserRating = userRating;
            repo.Edit(movie);
            var editedMovie = repo.Get(id);
            Assert.AreEqual(editedMovie.Title, title);
            Assert.AreEqual(editedMovie.DirectorName, director);
            Assert.AreEqual(editedMovie.UserRating, userRating);
        }

        [Test]
        public void CanDeleteMovie()
        {
            var repo = new MockMovieRepository();
            repo.Delete(2);
            var movies = repo.List();
            Assert.AreEqual(movies.ToList().Count, 2);
        }

    }
}
