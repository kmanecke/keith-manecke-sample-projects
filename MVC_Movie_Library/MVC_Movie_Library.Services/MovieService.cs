﻿using System;
using System.Linq;
using System.Collections.Generic;
using MVC_Movie_Library.Data.Factories;
using MVC_Movie_Library.Data.Interfaces;
using MVC_Movie_Library.Services.Interfaces;
using MVC_Movie_Library.Models;

namespace MVC_Movie_Library.Services
{
    public class MovieService : IMovieService
    {
        private IMovieRepository _repo;

        public MovieService()
        {
            _repo = MovieRepositoryFactory.GetMovieRepository();
        }

        public IEnumerable<Movie> List()
        {
            var result = _repo.List();
            return result;
        }

        public Movie Get(int id)
        {
            var result = _repo.Get(id);
            return result;
        }

        public void Add(Movie movie)
        {
            _repo.Add(movie);
        }

        public void Edit(Movie movie)
        {
            _repo.Edit(movie);
        }

        public void Delete(int id)
        {
            _repo.Delete(id);
        }

        public IEnumerable<Movie> Search(string searchStr)
        {
            var movies = _repo.List();
            var result = movies.Where(m => m.Title.ToLower().Contains(searchStr.ToLower())).ToList();
            return result;
        }
    }
}
