﻿using System;
using System.Collections.Generic;
using MVC_Movie_Library.Models;

namespace MVC_Movie_Library.Services.Interfaces
{
    public interface IMovieService
    {
        IEnumerable<Movie> List();
        Movie Get(int id);
        void Add(Movie movie);
        void Edit(Movie movie);
        void Delete(int id);
        IEnumerable<Movie> Search(string searchStr);
    }
}
