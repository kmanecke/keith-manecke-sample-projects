﻿var uri = 'https://localhost:44387/api/movies/';

$(document)
            .on('click', '.btnCheckInMovie',
                function () {
                    var id = $(this).attr('value');
                    loadReturnValues(id);
                });

function loadReturnValues(id) {
    $.getJSON(uri + id)
        .done(function (data) {
            if (data.BorrowerName == null) {
                alert('You cannot check-in a movie if it is not loaned out!');
            } else {
                $('#borrowerNameLoanModal').val('');
                $('#idReturnModal').val(data.MovieId);
                $('#titleReturnModal').text(data.Title);
                $('#borrowerReturnModal').text(data.BorrowerName);
                $('#borrowedDateReturnModal').text(data.BorrowedDate.substr(0, 10));
                $('#returnMovie').modal('show');

            }
        })
    .fail(function (jqXhr, status, err) {
        alert(status + " - " + err);
    });
}

$(document)
    .ready(function () {
        $('#btnSaveReturnChanges')
            .on('click',
                function () {
                    var returnMovieId = $('#idReturnModal').val();
                    
                    $.post(uri + 'loan/' + returnMovieId)
                     .done(function () {
                         if (window.location.href.toLowerCase().indexOf('search') !== -1) {
                             findMovies();
                         } else {
                             loadMovies();
                         }
                         $('#returnMovie').modal('hide');
                     })
                    .fail(function (jqXhr, status, err) {
                        alert(status + " - " + err);
                    });

                });
    });

