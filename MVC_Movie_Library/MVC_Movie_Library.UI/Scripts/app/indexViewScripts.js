﻿var uri = 'https://localhost:44387/api/movies/';

$(document)
    .ready(function () {
        loadMovies();
    });

function loadMovies() {
    $.getJSON(uri)
        .done(function (data) {
            $('.movies tbody tr').remove();
            $.each(data,
                function (index, movie) {
                    $(createRowForIndexList(movie)).appendTo($('.movies tbody'));
                });
        });
};

function createRowForIndexList(movie) {
    return '<tr><td>' +
        movie.MovieId +
        '</td><td>' +
        movie.Title +
        '</td><td>' +
        checkReleaseDate(movie.ReleaseDate) +
        '</td><td>' +
        movie.MpaaRating +
        '</td><td>' +
        checkBorrowerName(movie.BorrowerName) +
        '</td><td>' +
        checkBorrowedDate(movie.BorrowedDate) +
        '</td><td>' +
        '<button type="button" class="btn btn-primary btn-sm btnShowDetails" " value=' + movie.MovieId +
        '>View Details</button>' +
        '</td><td>' +
        '<button type="button" class="btn btn-warning btn-sm btnLoanMovie" value=' + movie.MovieId +
        '>Loan</button>' +
        '</td><td>' +
        '<button type="button" class="btn btn-success btn-sm btnCheckInMovie" value=' + movie.MovieId +
        '>Check-In</button>' +
        '</td><td>' +
        '<button type="button" class="btn btn-danger btn-sm btnDeleteMovie" value=' + movie.MovieId +
        '>Delete</button>' +
        '</td></tr>';
};

function checkReleaseDate(releaseDate) {
    if (releaseDate === null) {
        return "";
    } else {
        return releaseDate.substr(0, 4);
    }
}

