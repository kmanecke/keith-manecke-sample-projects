﻿var uri = 'https://localhost:44387/api/movies/';

$('#btnShowDetailsTest')
            .on('click',
                function () {
                    var id = $('#btnShowDetailsTest').attr('value');
                    loadDetailsTest(id);
                    $('#showDetailsTest').modal('show');

                });

function loadDetailsTest(id) {
    $.getJSON(uri + id)
        .done(function (data) {
            $('#sampleTitleTest').text(data.Title);
            $('#sampleRatingTest').text(data.MpaaRating);
            $('#sampleBorrowerTest').text(data.BorrowerName);
        })
                .fail(function (jqXhr, status, err) {
                    alert(status + " - " + err);
                });
};