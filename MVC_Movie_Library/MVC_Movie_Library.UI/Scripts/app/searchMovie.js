﻿var uri = 'https://localhost:44387/api/movies/';

$(document)
    .ready(function () {
        $('#btnSearchByTitle')
            .on('click',
                function() {
                    findMovies();
                });
    });

function findMovies() {
    var title = $('#title').val();

    $.getJSON(uri + 'search/' + title)
        .done(function(data) {
            if (data.length == 0) {
                $('.search-movies tbody tr').remove();
                $(createEmptyResultRow()).appendTo($('.search-movies tbody'));
            } else {
                $('.search-movies tbody tr').remove();
                $.each(data,
                    function (index, movie) {
                        $(createRowForIndexList(movie)).appendTo($('.search-movies tbody'));
                    });
            }
        })
        .fail(function (jqXhr, status, err) {
            $('#errorMessage thead tr').text('Error: ' + err);
        });
}

function createEmptyResultRow() {
    return '<tr><td>-</td><td>' +
        'None Found' +
        '</td><td colspan="8"></tr>';
};

