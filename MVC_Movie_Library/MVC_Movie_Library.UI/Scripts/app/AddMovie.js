﻿var uri = 'https://localhost:44387/api/movies/';

$(document)
    .ready(function() {
        $('#btnShowAddMovie')
            .on('click',
                function() {
                    $('#title').val('');
                    $('#releaseDate').val('');
                    $('#mpaaRating').val('');
                    $('#directorName').val('');
                    $('#studio').val('');
                    $('#userRating').val('');
                    $('#userNotes').val('');
                    $('#actors').val('');
                    $('#addMovieModal').modal('show');
                });

        $('#btnAddMovie')
            .on('click',
                function() {
                    var movie = {};
                    movie.Title = $('#title').val();
                    movie.ReleaseDate = $('#releaseDate').val();
                    movie.MpaaRating = $('#mpaaRating').val();
                    movie.DirectorName = $('#directorName').val();
                    movie.Studio = $('#studio').val();
                    movie.UserRating = $('#userRating').val();
                    movie.UserNotes = $('#userNotes').val();
                    movie.Actors = $('#actors').val();

                    $.post(uri + 'add/', movie)
                        .done(function() {
                            loadMovies();
                            $('#addMovieModal').modal('hide');
                        })
                        .fail(function(jqXhr, status, err) {
                            alert(status + " - " + err);
                        });
                });
    });