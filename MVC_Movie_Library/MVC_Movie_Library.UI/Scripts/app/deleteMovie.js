﻿var uri = 'https://localhost:44387/api/movies/';

$(document)
            .on('click', '.btnDeleteMovie',
                function () {
                    var id = $(this).attr('value');
                    loadDeleteValues(id);
                });

function loadDeleteValues(id) {
    $.getJSON(uri + id)
        .done(function (data) {
            $('#idDeleteModal').val(data.MovieId);
            $('#titleDeleteModal').text(data.Title);
            $('#deleteMovie').modal('show');

        })
    .fail(function (jqXhr, status, err) {
        alert(status + " - " + err);
    });
}
$(document)
    .ready(function () {
        $('#btnConfirmDeleteRecord')
           .on('click',
               function () {
                   var id = $('#idDeleteModal').val();
                   $.ajax({
                       url: uri + 'delete/' + id,
                       type: 'DELETE',
                       success: function (movie) {
                           deleteRow(movie);
                           loadMovies();

                       },
                       error: function (xhr, status, err) {
                           alert('error1: ' + err);
                       }
                   });

                   $('#deleteMovie').modal('hide');
               });
    });

function deleteRow(movie) {
    $('#movies tbody tr').remove(movie);
}