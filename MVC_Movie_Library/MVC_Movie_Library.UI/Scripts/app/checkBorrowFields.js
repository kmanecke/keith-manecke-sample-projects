﻿function checkBorrowerName(borrowerName) {
    if (borrowerName === null) {
        return "-- Available --";
    } else {
        return borrowerName;
    }
}

function checkBorrowedDate(borrowedDate) {
    if (borrowedDate === null) {
        return "-- Available --";
    } else {
        return borrowedDate.substr(0, 10);
    }
}

function checkReturnedDate(returnedDate, borrowedDate) {
    if (borrowedDate !== null) {
        return "-- Still Out --";
    } else {
        return "-- Available --";
    }
};