﻿var uri = 'https://localhost:44387/api/movies/';

$(document)
            .on('click', '.btnShowDetails',
                function () {
                    var id = $(this).attr('value');
                    loadDetails(id);
                    $('#showDetails').modal('show');
                    
                });

function loadDetails(id) {
    $.getJSON(uri + id)
        .done(function (data) {
            $('#sampleTitle').text(data.Title);
            $('#sampleReleaseDate').text(data.ReleaseDate.substr(0, 4));
            $('#sampleMpaaRating').text(data.MpaaRating);
            $('#sampleDirectorName').text(data.DirectorName);
            $('#sampleStudio').text(data.Studio);
            $('#sampleUserRating').text(data.UserRating + ' stars');
            $('#sampleUserNotes').text(data.UserNotes);
            $('#sampleActors').text(data.Actors);
            $('#sampleBorrowerName').text(checkBorrowerName(data.BorrowerName));
            $('#sampleBorrowedDate').text(checkBorrowedDate(data.BorrowedDate));
        })
                .fail(function (jqXhr, status, err) {
                    alert(status + " - " + err);
                });
};

