﻿var uri = 'https://localhost:44387/api/movies/';

$(document)
            .on('click', '.btnLoanMovie',
                function () {
                    var id = $(this).attr('value');
                    loadLoanValues(id);
                });

function loadLoanValues(id) {
    $.getJSON(uri + id)
        .done(function (data) {
            if (data.BorrowerName !== null) {
                alert('You cannot loan out a movie that is already on loan!');
            } else {
                $('#borrowerNameLoanModal').val('');
                $('#idLoanModal').val(data.MovieId);
                $('#titleLoanModal').text(data.Title);
                $('#loanMovie').modal('show');
                
            }
        })
    .fail(function (jqXhr, status, err) {
        alert(status + " - " + err);
    });
}

$(document)
    .ready(function () {
        $('#btnSaveLoanChanges')
            .on('click',
                function () {
                    var loanMovieId = $('#idLoanModal').val();
                    var loanBorrowerName = $('#borrowerNameLoanModal').val();

                    $.post(uri + 'loan/' + loanMovieId + '/' + loanBorrowerName)
                        .done(function () {
                            if (window.location.href.toLowerCase().indexOf('search') !== -1) {
                                findMovies();
                            } else {
                                loadMovies();
                            }
                         $('#loanMovie').modal('hide');
                     })
                    .fail(function (jqXhr, status, err) {
                        alert(status + " - " + err);
                    });

                });
    });

