﻿using System.Web;
using System.Web.Mvc;

namespace MVC_Movie_Library.UI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
