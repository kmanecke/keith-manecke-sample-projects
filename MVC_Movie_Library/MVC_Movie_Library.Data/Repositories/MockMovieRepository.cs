﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_Movie_Library.Data.Interfaces;
using MVC_Movie_Library.Models;

namespace MVC_Movie_Library.Data.Repositories
{
    public class MockMovieRepository : IMovieRepository
    {
        private static List<Movie> movies;

        static MockMovieRepository()
        {
            movies = new List<Movie>()
            {
                new Movie()
                {
                    MovieId = 1,
                    Title = "The Big Lebowski",
                    ReleaseDate = new DateTime(1998, 3, 6),
                    MpaaRating = "R",
                    DirectorName = "Joel Coen",
                    Studio = "Working Title Films, PolyGram Filmed Entertainment",
                    UserRating = 5,
                    UserNotes = "This movie is very funny!",
                    Actors = "Jeff Bridges, John Goodman, Julianne Moore",
                    BorrowerName = "Doug Douglas",
                    BorrowedDate = new DateTime(2020, 8, 20)
                    },
                new Movie()
                {
                    MovieId = 2,
                    Title = "Primer",
                    ReleaseDate = new DateTime(2004, 10, 8),
                    MpaaRating = "PG-13",
                    DirectorName = "Shane Carruth",
                    Studio = "Independently Produced",
                    UserRating = 5,
                    UserNotes = "This movie blew my mind, man!",
                    Actors = "Shane Carruth, David Sullivan, Casey Gooden",
                    BorrowerName = "Joe Jones",
                    BorrowedDate = new DateTime(2020, 9, 17),
                    ReturnedDate = new DateTime(2020, 9, 19)
                },
                new Movie()
                {
                    MovieId = 3,
                    Title = "Meatballs",
                    ReleaseDate = new DateTime(1979, 6, 29),
                    MpaaRating = "PG",
                    DirectorName = "Ivan Reitman",
                    Studio = "Paramount Pictures",
                    UserRating = 3,
                    UserNotes = "Had some slow parts, but overall pretty solid.",
                    Actors = "Bill Murray, Kate Lynch, Chris Makepeace"
                    },
                new Movie()
                {
                    MovieId = 4,
                    Title = "Selma",
                    ReleaseDate = new DateTime(2016, 6, 29),
                    MpaaRating = "PG",
                    DirectorName = "Ava DuVernay",
                    Studio = "Sony",
                    UserRating = 5,
                    UserNotes = "Well done.",
                    Actors = "David Oyelowo, Carmen Ejogo, Tim Roth"
                    },
                new Movie()
                {
                    MovieId = 5,
                    Title = "Jaws",
                    ReleaseDate = new DateTime(1975, 6, 1),
                    MpaaRating = "PG",
                    DirectorName = "Steven Spielberg",
                    Studio = "Paramount",
                    UserRating = 3,
                    UserNotes = "Has some good moments. The shark looked fake though...",
                    Actors = "Roy Scheider, Richard Dreyfuss"
                }
            };
        }

        public IEnumerable<Movie> List()
        {
            return movies;
        }

        public Movie Get(int movieId)
        {
            return movies.FirstOrDefault(d => d.MovieId == movieId);
        }

        public void Add(Movie movie)
        {
            var num = movies.Max(d => d.MovieId) + 1;
            movie.MovieId = num;
            movies.Add(movie);
        }

        public void Edit(Movie movie)
        {
            var movieToUpdate = movies.First(d => d.MovieId == movie.MovieId);
            movieToUpdate.Title = movie.Title;
            movieToUpdate.ReleaseDate = movie.ReleaseDate;
            movieToUpdate.MpaaRating = movie.MpaaRating;
            movieToUpdate.DirectorName = movie.DirectorName;
            movieToUpdate.Studio = movie.Studio;
            movieToUpdate.UserRating = movie.UserRating;
            movieToUpdate.UserNotes = movie.UserNotes;
            movieToUpdate.Actors = movie.Actors;
            movieToUpdate.BorrowerName = movie.BorrowerName;
            movieToUpdate.BorrowedDate = movie.BorrowedDate;
            movieToUpdate.ReturnedDate = movie.ReturnedDate;
        }

        public void Delete(int movieId)
        {
            var movieToDelete = movies.First(d => d.MovieId == movieId);
            movies.Remove(movieToDelete);
        }
    }
}
