﻿using System;
using System.Collections.Generic;
using MVC_Movie_Library.Data.Interfaces;
using MVC_Movie_Library.Models;

namespace MVC_Movie_Library.Data.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        public IEnumerable<Movie> List()
        {
            throw new NotImplementedException();
        }

        public Movie Get(int movieId)
        {
            throw new NotImplementedException();
        }

        public void Add(Movie movie)
        {
            throw new NotImplementedException();
        }

        public void Edit(Movie movie)
        {
            throw new NotImplementedException();
        }

        public void Delete(int movieId)
        {
            throw new NotImplementedException();
        }
    }
}
