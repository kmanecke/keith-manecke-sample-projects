﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_Movie_Library.Models;

namespace MVC_Movie_Library.Data.Interfaces
{
    public interface IMovieRepository
    {
        IEnumerable<Movie> List();
        Movie Get(int movieId);
        void Add(Movie movie);
        void Edit(Movie movie);
        void Delete(int movieId);
    }
}
