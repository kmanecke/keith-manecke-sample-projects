﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_Movie_Library.Data.Interfaces;
using MVC_Movie_Library.Data.Repositories;

namespace MVC_Movie_Library.Data.Factories
{
    public static class MovieRepositoryFactory
    {
        public static IMovieRepository GetMovieRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "Prod":
                    return new MovieRepository();
                case "Test":
                    return new MockMovieRepository();
                default: 
                    throw new ArgumentException();
            }
        }
    }
}
