﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MVC_Movie_Library.Services;
using MVC_Movie_Library.Services.Interfaces;
using MVC_Movie_Library.Models;

namespace MVC_Movie_Library.UI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MoviesController : ApiController
    {
        private readonly IMovieService movieService;

        public MoviesController()
        {
            movieService = new MovieService();
        }

        public IEnumerable<Movie> Get()
        {
            var svc = new MovieService();
            return svc.List();
        }

        [Route("api/movies/search/{title}")]
        [HttpGet]
        public IEnumerable<Movie> SearchByTitle(string title)
        {
            var movies = movieService.Search(title);
            return movies;
        }

        [HttpGet]
        public Movie GetById(int id)
        {
            return movieService.Get(id);
        }

        [Route("api/movies/add/")]
        [HttpPost]
        public HttpResponseMessage PostAddMovie(Movie movie)
        {
            movieService.Add(movie);
            var response = Request.CreateResponse(HttpStatusCode.Created);
            return response;
        }

        [Route("api/movies/loan/{id}/{name}")]
        [HttpPost]
        public HttpResponseMessage PostLoanDetails(int id, string name)
        {
            var movieRecord = movieService.Get(id);
            movieRecord.BorrowedDate = DateTime.Now;
            movieRecord.BorrowerName = name;
            movieService.Edit(movieRecord);
            var response = Request.CreateResponse(HttpStatusCode.Created);
            return response;
        }

        [Route("api/movies/loan/{id}")]
        [HttpPost]
        public HttpResponseMessage PostReturnDetails(int id)
        {
            var movieRecord = movieService.Get(id);
            movieRecord.ReturnedDate = DateTime.Now;
            movieRecord.BorrowedDate = null;
            movieRecord.BorrowerName = null;
            movieService.Edit(movieRecord);
            var response = Request.CreateResponse(HttpStatusCode.Created);
            return response;
        }

        [Route("api/movies/delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteReturnDetails(int id)
        {
            movieService.Delete(id);
            var response = Request.CreateResponse(HttpStatusCode.Created);
            return response;
        }
    }
}
