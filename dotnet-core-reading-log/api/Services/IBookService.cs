using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using data;
using models;

namespace api.Services 
{
    public interface IBookService
    {
        Task<IList<Book>> GetBooks();
        Task<Book> GetBook(int bookID);
        Task AddBook(Book book);
        Task EditBook(Book book);
        Task DeleteBook(int bookID);
    }
}