using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using data;
using models;
using models.Interfaces.Repositories;

namespace api.Services 
{
    public class BookService : IBookService 
    {
        private readonly ILogger logger;
        private readonly IBookRepository repository;

        public BookService(ILogger<BookService> logger, IBookRepository repository) 
        {
            this.logger = logger;
            this.repository = repository;
        }

        public async Task<IList<Book>> GetBooks()
        {
            return await repository.GetBooks();
        }

        public async Task<Book> GetBook(int bookID)
        {
            return await repository.GetBook(bookID);
        }

        public async Task AddBook(Book book)
        {
            await repository.AddBook(book);
        }

        public async Task EditBook(Book book)
        {
            await repository.EditBook(book);
        }

        public async Task DeleteBook(int bookID)
        {
            await repository.DeleteBook(bookID);
        }
    }
}