using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using data;
using models;
using models.Interfaces.Repositories;

namespace api.Services 
{
    public class AuthorService : IAuthorService 
    {
        private readonly ILogger logger;
        private readonly IAuthorRepository repository;

        public AuthorService(ILogger<AuthorService> logger, IAuthorRepository repository) 
        {
            this.logger = logger;
            this.repository = repository;
        }

        public async Task<IList<Author>> GetAuthors()
        {
            return await repository.GetAuthors();
        }

        public async Task<Author> GetAuthor(int authorID)
        {
            return await repository.GetAuthor(authorID);
        }

        public async Task AddAuthor(Author author)
        {
            await repository.AddAuthor(author);
        }

        public async Task EditAuthor(Author author)
        {
            await repository.EditAuthor(author);
        }

        public async Task DeleteAuthor(int authorID)
        {
            await repository.DeleteAuthor(authorID);
        }
    }
}