using System;
using System.Collections.Generic;
using System.Linq;
using models;

namespace data.tests.TestData 
{
    public class ReadingLogTestData : IReadingLogTestData
    {
        public IList<Book> Books { get; set; }
        public IList<Author> Authors { get; set; }
        
        public ReadingLogTestData() 
        {
            Authors = new List<Author>() 
            {
                new Author() 
                {
                    ID = 10,
                    Name = "John Doe"
                },
                new Author() 
                {
                    ID = 11,
                    Name = "Jane Doe"
                }
            };

            Books = new List<Book>() 
            {
                new Book() 
                {
                    ID = 1,
                    Title = "Test Book 1",
                    DateFinished = new DateTime(2018, 05, 07),
                    Comment = "No comment",
                    AuthorID = 10
                },
                new Book() 
                {
                    ID = 2,
                    Title = "Test Book 2",
                    DateFinished = null,
                    Comment = "Didn't finish",
                    AuthorID = 11
                }
            };
        }
    }
}