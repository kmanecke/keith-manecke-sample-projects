using System;
using Xunit;
using System.Linq;
using data.tests.Fixtures;
using data.Repositories;
using models;
using models.Interfaces.Repositories;

namespace data.tests.RepositoryTests  
{
    public class AuthorRepositoryTests : IClassFixture<ReadingLogFixture> 
    {
        private ReadingLogFixture fixture;
        private IAuthorRepository repository;

        public AuthorRepositoryTests(ReadingLogFixture fixture) 
        {
            this.fixture = fixture;
            repository = new AuthorRepository(fixture.context);
        }

        [Fact]
        public async void GetAuthorsReturnsAllAuthorRecords() 
        {
            var authors = await repository.GetAuthors();
            Assert.Equal(2, authors.Count);
        }

        [Fact]
        public async void GetAuthorReturnsCorrectAuthorRecord() 
        {
            var author = await repository.GetAuthor(10);
            Assert.Equal("John Doe", author.Name);
        }

        [Fact]
        public async void AddAuthorSucceeds() 
        {
            var author = new Author() 
            {
                ID = 25,
                Name = "Great Writer"
            };

            await repository.AddAuthor(author);

            var newAuthor = await repository.GetAuthor(25);
            Assert.Equal("Great Writer", newAuthor.Name);
        }

        [Fact]
        public async void EditAuthorSucceeds() 
        {
            var author = await repository.GetAuthor(10);
            author.Name = "Foo Bar";

            await repository.EditAuthor(author);

            var updatedAuthor = await repository.GetAuthor(10);
            Assert.Equal("Foo Bar", updatedAuthor.Name);
        }

        [Fact]
        public async void DeleteAuthorSucceeds() 
        {
            await repository.DeleteAuthor(11);

            var authors = await repository.GetAuthors();
            Assert.Equal(1, authors.Count);
        }
    }
}