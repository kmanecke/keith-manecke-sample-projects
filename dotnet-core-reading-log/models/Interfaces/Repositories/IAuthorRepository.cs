using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using models;

namespace models.Interfaces.Repositories 
{
    public interface IAuthorRepository
    {
        Task<IList<Author>> GetAuthors();
        Task<Author> GetAuthor(int authorID);
        Task AddAuthor(Author author);
        Task EditAuthor(Author author);
        Task DeleteAuthor(int authorID);    
    }
}