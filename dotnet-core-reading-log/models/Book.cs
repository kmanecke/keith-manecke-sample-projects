﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace models
{
    public class Book
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public DateTime? DateFinished { get; set;}
        public string Comment { get; set; }
        public int? AuthorID { get; set; }
        public Author Author { get; set; }
    }
}
