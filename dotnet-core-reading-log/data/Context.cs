﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using models;

namespace data
{
    public class ReadingLogContext : DbContext 
    {
        public ReadingLogContext(DbContextOptions<ReadingLogContext> options) 
            : base(options) 
        { }
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }

       protected override void OnModelCreating(ModelBuilder modelBuilder)
       {
           modelBuilder.Entity("models.Book", b =>
                {
                    b.HasOne("models.Author", "Author")
                        .WithMany("Books")
                        .HasForeignKey("AuthorID")
                        .OnDelete(DeleteBehavior.SetNull);
                });
       }
    }
}
