using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using models;
using models.Interfaces.Repositories;

namespace data.Repositories 
{
    public class AuthorRepository : IAuthorRepository 
    {
        private readonly ReadingLogContext context;
        
        public AuthorRepository(ReadingLogContext context) 
        {
            this.context = context;
        }

        public async Task<IList<Author>> GetAuthors()
        {
            return await context.Authors.OrderBy(a => a.Name).ToListAsync();
        }

        public async Task<Author> GetAuthor(int authorID)
        {
            var result = await context.Authors.FindAsync(authorID);

            if (result == null)
                throw new EntityNotFoundException("Author", authorID);

            return result;
        }

        public async Task AddAuthor(Author author)
        {
            await context.Authors.AddAsync(author);
            await context.SaveChangesAsync();
        }

        public async Task EditAuthor(Author author) 
        {
            var authorToUpdate = await context.Authors.FindAsync(author.ID);
            authorToUpdate.Name = author.Name;
            await context.SaveChangesAsync();
        }

        public async Task DeleteAuthor(int authorID)
        {
            var authorToRemove = await context.Authors.FindAsync(authorID);

            if (authorToRemove == null)
                throw new EntityNotFoundException("Author", authorID);

            context.Authors.Remove(authorToRemove);
            await context.SaveChangesAsync();
        }
    }
}