﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
var authorUri = 'https://localhost:5001/api/author/';
var bookUri = 'https://localhost:5001/api/book/';

$(document)
    .ready(function () {
        loadBooks();

        $('#btnShowAddBook')
            .on('click', 
                function() {
                    if (loadAuthorSelectList('#ddlAuthorAdd')) {
                        $('#addBookDetails').modal('show');
                    };
                }
            );

        $('#btnManageAuthors')
            .on('click', 
                function() {
                    loadAuthors();
                    $('#add-author').val('');
                    $('#manageAuthors').modal('show');
                }
            );
    });

//event handlers for dynamically generated content
    $(document)
        .on('click', '.btnShowEditBook',
            function() {
                var id = $(this).attr('value');
                var authorList = $('#ddlAuthorEdit');
                authorList.find('option').not(':first').remove();
                loadEditDetails(id);
                $('#editBookDetails').modal('show');        
            }
        );

    $(document)
        .on('click', '#btnSaveChanges', 
            function() {
                editBook();
            }
        );
    
    $(document)
        .on('click', '#btnAddBook',
            function() {
                addBook();
            }
        );

    $(document)
        .on('click', '.btnDeleteBook',
            function() {
                var response = confirm("Are you sure you want to delete this book?");
                if (response) {
                    var id = $(this).attr('value');
                    deleteBook(id);    
                }
            }
        );

    $(document)
        .on('click', '#btnAddAuthor',
            function() {
                var newName = $('#add-author').val();
                if (newName) {
                    addAuthor(newName);
                } else {
                    alert("You must enter a name value for the New Author");
                }
            }
        );

    $(document)
        .on('click', '.btnDeleteAuthor',
            function() {
                var response = confirm("Are you sure you want to delete this author?");
                if (response) {
                    var id = $(this).attr('value');
                    deleteAuthor(id);    
                }
            }
        );

//book
    function loadBooks() {
        $.getJSON(bookUri)
            .done(function (data) {
                $('#book-list tbody tr').remove();
                $.each(data,
                    function (index, book) {
                        $(createRowForBookList(book)).appendTo($('#book-list tbody'));
                    });
            })
            .fail(function (jqXhr, status, err) {
                alert(status + " - " + err);
            });
    };

    function createRowForBookList(book) {
        var tdBreak = '</td><td>';
        var name = book.authorID ? book.author.name : 'null';
        var dateFinished = book.dateFinished ? book.dateFinished.substr(0, 10) : 'null';
        return '<tr><td>' + 
            book.title + 
            tdBreak + 
            name + 
            tdBreak +
            dateFinished +
            tdBreak + 
            book.comment +
            tdBreak + 
            '<button type="button" class="btn btn-primary btn-sm btnShowEditBook" " value=' + 
            book.id +
            '>Edit Details</button>' +
            tdBreak + 
            '<button type="button" class="btn btn-danger btn-sm btnDeleteBook" value=' + 
            book.id +
            '>Delete</button>' +
            '</td></tr>';
    };

    function loadEditDetails(id) {
        if (loadAuthorSelectList('#ddlAuthorEdit')) {
            $.getJSON(bookUri + id)
            .done(function (data) {
                $('#edit-id').val(data.id);
                $('#edit-title').val(data.title);
                if (data.authorID) {
                    $('#ddlAuthorEdit').val(data.author.id);
                };        
                $('#edit-date-finished').val(data.dateFinished.substr(0, 10));
                $('#edit-comments').val(data.comment);
            })
            .fail(function (jqXhr, status, err) {
                alert(status + " - " + err);
            });
        };
    }

    function addBook() {
        var book = {};
        book.title = $('#add-title').val();
        book.dateFinished = $('#add-date-finished').val();
        book.comment = $('#add-comments').val();
        if ($('#ddlAuthorAdd').val() > 0) {
            book.authorID = $('#ddlAuthorAdd').val();
        }

        $.ajax({
            method: 'POST',
            data: JSON.stringify(book),
            url: bookUri,
            contentType: 'application/json'
            })
            .done(function() {
                loadBooks();
            })
            .fail(function(jqXhr, status, err) {
                alert(status + " - " + err);
        });

        $('#addBookDetails').modal('hide');
    }

    function editBook() {
        var book = {};
        book.id = $('#edit-id').val();
        book.title = $('#edit-title').val();
        book.dateFinished = $('#edit-date-finished').val();
        book.comment = $('#edit-comments').val();
        book.authorID = $('#ddlAuthorEdit').val();

        $.ajax({
            method: 'POST',
            data: JSON.stringify(book),
            url: bookUri + 'edit/',
            contentType: 'application/json'
            })
            .done(function() {
                loadBooks();
            })
            .fail(function(jqXhr, status, err) {
                alert(status + " - " + err);
        });

        $('#editBookDetails').modal('hide');
    }

    function deleteBook(bookID) {
        $.ajax({
            method: 'DELETE',
            url: bookUri + 'delete/' + bookID,
        })
        .done(function() {
            loadBooks();
        })
        .fail(function (jqXhr, status, err) {
            alert(status + " - " + err);
        });
    };

//author
    function loadAuthors() {
        $.getJSON(authorUri)
            .done(function (data) {
                $('#author-list tbody tr').remove();
                $.each(data,
                    function (index, author) {
                        $(createRowForAuthorList(author)).appendTo($('#author-list tbody'));
                    });
            })
            .fail(function (jqXhr, status, err) {
                alert(status + " - " + err);
            });
        };

    function createRowForAuthorList(author) {
        return '<tr><td>' + 
            author.name + 
            '</td><td>' + 
            '<button type="button" class="btn btn-danger btn-sm btnDeleteAuthor" value=' + 
            author.id +
            '>Delete</button>' +
            '</td></tr>';
        };

    function loadAuthorSelectList(ddlName) {
        var success = true;
        $.getJSON(authorUri)
        .done(function (data) {
            $.each(data, 
                function(i, author) {
                    $(ddlName)
                        .append($('<option>', {
                            value: author.id,
                            text: author.name
                    }));
                });
        })
        .fail(function (jqXhr, status, err) {
            alert(status + " - " + err);
            success = false;
        });
        return success;
    }

    function addAuthor(newName) {
        var author = { name : newName };
        $.ajax({
            method: 'POST',
            data: JSON.stringify(author),
            url: authorUri,
            contentType: 'application/json'
            })
            .done(function() {
                loadAuthors();
            })
            .fail(function(jqXhr, status, err) {
                alert(status + " - " + err);
        });
    };

    function deleteAuthor(authorID) {
        $.ajax({
            method: 'DELETE',
            url: authorUri + 'delete/' + authorID,
        })
        .done(function() {
            loadAuthors();
            loadBooks();
        })
        .fail(function (jqXhr, status, err) {
            alert(status + " - " + err);
        });
    };
