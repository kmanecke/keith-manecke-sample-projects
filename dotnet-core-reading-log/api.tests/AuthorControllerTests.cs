using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Moq;
using models;
using api.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace api.tests
{
    public class AuthorControllerTests
    {
        [Fact]
        public void GetReturnsProductList()
        {
            //arrange
            var mockService = new Mock<api.Services.IAuthorService>();
            mockService.Setup(x => x.GetAuthor(10)).Returns(new Task<Author>(() => new Author { ID = 10, Name = "Keith" }));
            var controller = new AuthorController(mockService.Object);

            //act 
            var actionResult = controller.GetAuthor(10);
            var contentResult = actionResult as OkObjectResult;

            //assert 
            Assert.NotNull(contentResult);
            Assert.NotNull(contentResult.Value);
            Assert.Equal(200, contentResult.StatusCode.Value);
            //Assert.Equal(10, (Author)contentResult)
        }
    }
}
