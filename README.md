# README #

Sample projects for Keith Manecke, .NET Developer in Columbus, Ohio. 

Notes on some of the projects included: 

### Reading Log - dotnet core: 
Sample project for getting familiar with dotnet core; features covered include: 

* dotnet core dependency injection

* entity framework core 

* wep api 

* xunit test fixtures and unit testing

* exception filtering

* mvc and bootstrap in the web/ui


### MVC_Movie_Library: 

* ASP.NET MVC application for managing a personal movie library that allows adding, removing, loaning, and checking-in movies. 

* Utilizes MVC framework, HTML, CSS, Bootstrap, JavaScript, jQuery, and Web API functionality.

* The list of movies is in a mock repository that is currently hard-coded into the application; added movies and loan details are stored in-memory.